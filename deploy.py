import logging
import os
import shutil
import sys

from buildtools import os_utils, log

os_utils.safe_rmtree('dist')
os_utils.ensureDirExists('dist/ARMS/Data/Scripts')
os_utils.copytree('Content','dist/ARMS', verbose=True)
os_utils.copytree('src','dist/ARMS/Data/Scripts', verbose=True, ignore=['.csproj','.sln','bin/','obj/','.vs/'])