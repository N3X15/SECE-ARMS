﻿// skip file on build (different meaning for Help.cs)

using System;
using System.Collections.Generic;
using System.Text;
using Sandbox.ModAPI;

namespace Rynchodon.Autopilot
{
	/// <summary>
	/// commands here are help commands / topics
	/// </summary>
	public class Help
	{
		private const string pri_designator = "/arms help";
		private const string alt_designator = "/autopilot help";

		private struct Command
		{
			public string command;
			public string commandExt;
			public string description;

			public Command(string fromReadme)
			{
				fromReadme = fromReadme.Replace("[b]", string.Empty);
				fromReadme = fromReadme.Replace("[/b]", string.Empty);

				this.command = fromReadme.Split(' ')[0].Trim().ToUpper();
				this.commandExt = fromReadme.Split(':')[0].Trim();
				this.description = fromReadme.Trim();
			}
		}

		private Dictionary<string, LinkedList<Command>> help_messages;

		private Logger myLogger = new Logger(null, "Help") { MinimumLevel = Logger.severity.DEBUG };

		private bool initialized = false;

		private void initialize()
		{
			myLogger.debugLog("initializing", Logger.severity.TRACE);
			help_messages = new Dictionary<string, LinkedList<Command>>();
			List<Command> allCommands = new List<Command>();

allCommands.Add(new Command(@"C <x>, <y>, <z> : for flying to specific world coordinates.
Example - [ C 0, 0, 0 : C 500, 500, 500 ] - fly to {0, 0, 0} then fly to {500, 500, 500}, will keep flying back and forth
"));
allCommands.Add(new Command(@"C GPS:<name>:<x>:<y>:<z>: : GPS tags will be replaced with the appropriate coordinates. Keep in mind that GPS tags end with a colon which autopilot will not treat as a command separator.
Example - [ C GPS:Origin:1:2:3: ] - becomes [ C 1,2,3 ]
"));
allCommands.Add(new Command(@"Character, <name> : Fly to a character, <name> is the character's name
Example - [ Character, Rynchodon ] - Send your ship to me
"));
allCommands.Add(new Command(@"Char, <name> : Same as Character, <name>
"));
allCommands.Add(new Command(@"DISABLE : Disable ""ARMS Autopilot"" for the Autopilot, does not bring it to a stop.
Example - [ C 0, 0, 0 : Disable ] - Upon reaching {0, 0, 0}, will turn off and drift away...
Example - [ C 0, 0, 0 : STOP : Disable ] - Upon reaching {0, 0, 0}, will come to a stop and then turn off
Example - [ L Connector ; B Connector ; G Red Ship ; Disable ] - Will switch Autopilot off after docking.
"));
allCommands.Add(new Command(@"EXIT : Identical to stop, followed by disable
Example - [ C 0, 0, 0 : EXIT ] - Upon reaching {0, 0, 0}, will come to a stop and then turn off
"));
allCommands.Add(new Command(@"G <name> : fly towards a specific friendly grid, by name of grid (Ship Name)
Example - [ G Platform : EXIT ] - Fly to a friendly grid that has ""Platform"" in its name, then stop
"));
allCommands.Add(new Command(@"H <ore>, ... : Extract specified ores from an asteroid. All ores are treated the same; if you wish to prioritize, use multiple commands. Ores can be specified by ore name or chemical symbol. Ore detectors must have a two-way connection to the Autopilot block, see Antenna Relay and Radar.
Example - [ H Gold, Silver ] - Harvest either gold or silver, whichever is found first.
Example - [ H Gold ; H Silver ] - Harvest silver only if gold cannot be found.
"));
allCommands.Add(new Command(@"Harvest : Extract any ore from an asteroid. Ore detectors must have a two-way connection to the Autopilot block, see Antenna Relay and Radar.
Example - [ C 0,0,0 : Harvest ] - fly to {0,0,0} then harvest any ore.
"));
allCommands.Add(new Command(@"R <distance> : Recycle (grind) enemy and unowned ships within <distance> of the autopilot. Recycling will stop if grinders are damaged or full. NOTE: Only one recycler will be able to target each grid.
Example - [ R 1000 ] - Grind any enemy within 1 km.
Example - [ L Connector ; B Connector ; G Base ; W 1 m ; Unlock ; R 5 km ]
"));
allCommands.Add(new Command(@"STOP : Stop the ship before executing any more commands. Clears all commands except P and V.
"));
allCommands.Add(new Command(@"W <seconds> : Wait before travelling to the next destination. Can use m for minutes, or h for hours.
Example - [ C 0, 0 , 0 : W 60 : C 500, 500, 500 : EXIT ] - Will wait for 60 seconds after reaching {0, 0, 0}
Example - [ W 24 h ] - Wait for a day
"));
allCommands.Add(new Command(@"A <block>, <action> : Run an action on one or more blocks. <action> is case-sensitive. Autopilot will find every block that contains <block>, find the ITerminalAction that matches <action>, and apply it. Block must have faction share with Autopilot block's owner.
Example - [ A Thrust, OnOff_On ] - turn all the thrusters on
"));
allCommands.Add(new Command(@"Asteroid : Disable asteroid collision avoidance, only affects the next destination.
Example - [ Asteroid : C 0,0,0 : C 1000,0,0 ] - fly to 0,0,0 ignoring asteroids, fly to 1000,0,0 avoiding asteroids
"));
allCommands.Add(new Command(@"B <name> : for navigating to a specific block on a grid, will only affect the next use of G
Example - [ B Antenna : G Platform ] - fly to Antenna on Platform
"));
allCommands.Add(new Command(@"B <name>, <f> : <f> indicates which direction to face the landing block when landing
Example - [ L Landing Gear : B Beacon, Right : G Platform : Disable ] - Attach landing gear to the left side of beacon on Platform
"));
allCommands.Add(new Command(@"B <name>, <f>, <u> : <f> will be used as forward direction and <u> will be used as upward direction
"));
allCommands.Add(new Command(@"E <distance>, <response> : Start searching for an enemy. Use 0 for infinite distance. Use OFF to disable.
"));
allCommands.Add(new Command(@"E <distance>, Fight : Use weapons to attack enemy ship, will stop attack if autopilot's weapons are disabled.
Example - [ E 0, Fight ] - Fight any detected enemy.
"));
allCommands.Add(new Command(@"E <distance>, Flee : Escape from enemy, will stop fleeing if thrusters become disabled.
Example - [ E 2000, Flee ] - Flee from any enemy that comes within 2 km.
Example - [ E 0, Fight ; E 2000, Flee ] - Fight any enemy that is detected. If weapons are disabled, flee from any enemy within 2 km.
"));
allCommands.Add(new Command(@"E <distance>, Ram : Ram the enemy, will stop trying to ram if thrusters become disabled.
Example - [ E 0, Fight, Ram ] - If weapons become disabled while fighting, ram the enemy.
"));
allCommands.Add(new Command(@"E <distance>, Self-Destruct : Starts the countdown on all warheads on autopilot's ship.
Example - [ E 100, Self-Destruct ] : Explode if an enemy comes within 100 m.
Example - [ E 0, Fight, Ram, Self-Destruct ] - Fight the enemy. If weapons become disabled, ram the enemy. If thrusters become disabled, explode.
"));
allCommands.Add(new Command(@"E <distance>, ID <entityID>, <response> : An autopilot can be instructed to respond to a specific enemy using the entity ID of the enemy. Other enemies will be ignored.
Example - [ E 0, ID 12345, Fight ] - Fight the enemy with ID 12345.
"));
allCommands.Add(new Command(@"F <r>, <u>, <b> : fly a distance relative to self. coordinates are rightward, upward, backwards
Example - [ F 0, 0, -1000 ] - fly 1000m forward
"));
allCommands.Add(new Command(@"F <distance> <direction>, ... : generic form is a comma separated list of distances and directions
Example - [ F 1000 forward ] - fly 1000m forward
Example - [ F 1000 forward, 200 downward ] - fly to a point 1000m ahead and 200m below Autopilot block
"));
allCommands.Add(new Command(@"Form : Stay in formation with friendly grid (G command). Commands that occur after G will never be executed.
Example - [ N Autopilot, F, U ; B Cockpit, F, U ; Form ; G Convoy ] - Fly to Convoy, orient Autopilot to match Cockpit, and stay in formation
"));
allCommands.Add(new Command(@"L <block name> : Landing block. Autopilot will attempt to land the block on the target. Unless landing block is a Landing Gear, you must specify a target block with B.
Example - [ L Connector : B Connector : G Platform ] - attach local connector to connector on Platform
Example - [ N Laser Antenna : L Connector : B Connector : G Platform ] - Autopilot will keep ""Laser Antenna"" pointed at its target until it gets close enough to the target to start docking. Then Autopilot will attach the local connector to connector on Platform.
"));
allCommands.Add(new Command(@"L <name>, <f> : <f> is the direction of the landing block that will be treated as the front of the block
"));
allCommands.Add(new Command(@"L <name>, <f>, <u> : <u> is the direction of the landing block that will be used as upward.
"));
allCommands.Add(new Command(@"LAND, <Asteroid/Planet> : Land in the closest asteroid or planet. You must have specified a landing block.
Example - [ Land, Asteroid ] - Land the ship on the closest asteroid.
"));
allCommands.Add(new Command(@"LINE : Autopilot will attempt to fly in a straight line towards the next destination. Useful for flying through doors that open when approached or navigating inside a tight area.
"));
allCommands.Add(new Command(@"N <block name> : Navigation block. If the block is a Solar Panel or an Oxygen Farm, face it towards the sun. If the block is a laser antenna, face it towards its target. N may be combined with B and G to orient the ship relative to the B block.
Example - [ N Solar Panel ] - Face Solar Panel towards the sun.
Example - [ N Laser Antenna : C 0,0,0 ] - Face ""Laser Antenna"" towards its target coordinates and fly the ship to {0,0,0}
"));
allCommands.Add(new Command(@"N <block name>, <f> : The direction <f> will be treated as the forward direction of the block.
Example - [ N Laser Antenna, Back ] - Face the back of ""Laser Antenna"" towards its target
Example - [ N Cockpit, Forward ; B Antenna, Backward ; G Platform ] - Fly to Platform and face Cockpit's forward in the same direction as Antenna's backward.
"));
allCommands.Add(new Command(@"N <block name>, <f>, <u> : The direction <u> will be treated as the upward direction of the block.
Example - [ N Cockpit, F, D ; B Antenna, B, R ; G Platform ] - Fly to Platform and face Cockpit's forward in the same direction as Antenna's backward and face Cockpit's Downward in the same direction as Antenna's Rightward.
"));
allCommands.Add(new Command(@"O <r>, <u>, <b> : destination offset, only works if there is a block target, cleared after reaching destination. coordinates are right, up, back
Example - [ O 0, 500, 0 : B Antenna : G Platform ] - fly to 500m above Antenna on Platform
"));
allCommands.Add(new Command(@"O <distance> <direction>, ... : generic form is a comma separated list of distances and directions
Example - [ O 500 upward : B Antenna : G Platform ] - fly to 500m above Antenna on Platform
Example - [ O 100 forward, 500 upward : B Antenna : G Platform ] - fly to 100m ahead of and 500m above Antenna
"));
allCommands.Add(new Command(@"Orbit, <target> : Flies in circles around an entity, <target> can be ""Asteroid"", ""Planet"", or the name of a grid. The orbit starts from the current position of the Autopilot. For a planet, the Autopilot will attempt to achieve a true orbit.
Example - [ Orbit, Asteroid ] - Orbit the nearest asteroid.
Example - [ Orbit, Planet ] - Orbit the nearest planet.
Example - [ O 500 upward ; B Antenna ; G Platform ; Orbit, Platform ] - fly to 500m above Antenna on Platform, then orbit Platform.
"));
allCommands.Add(new Command(@"P <distance> : how close the grid needs to fly to the destination, default is 100m. For landing, used as the holding distance.
Example - [ P 10 : F 0, 0, -100 ] - Fly 100m forward, to within 10m
"));
allCommands.Add(new Command(@"T <name> : fetch commands from the public text of a text panel named <name>, starting at the first [ and ending at the first following ]
Example - [ T Command Panel ] - fetch commands from ""Command Panel""
"));
allCommands.Add(new Command(@"T <name>, <sub> : as above, the search for [ and ] will begin after the first occurrence of <sub>. It is recommend to make <sub> unique, so that it will not be confused with anything else.
Example - [ T Command Panel, {Line 4} ] - where ""Command Panel"" contains ... {Line 4} [ C 0,0,0 ] ... fly to {0,0,0}
"));
allCommands.Add(new Command(@"U <name> : unlock the specified block and move away from the attached object
Example - [ U Landing Gear ]
"));
allCommands.Add(new Command(@"U <name>, <f>, <u> : If directions were used to land the block, they should also be used to unland it.
"));
allCommands.Add(new Command(@"Unlock : unlock the most recently landed / docked block and move away from the attached object. The L command can also be used in combination with Unlock.
"));
allCommands.Add(new Command(@"Unland : alias for Unlock
"));
allCommands.Add(new Command(@"Undock : alias for Unlock
Example - [ L Landing Gear ; B Landing Pad, D ; G Platform ; W 60 ; Unlock ] - Attach Landing Gear to top of Landing Pad, wait 60 seconds, detach and move away.
Example - [ L Landing Gear ; Unlock ] - Detach Landing Gear from whatever it is connected to and move away.
"));
allCommands.Add(new Command(@"V <speed> : Autopilot will fly at the specified speed
Example - [ V 10 : C 0, 0, 0 : C 500, 500, 500 ] - fly back and forth between {0, 0, 0} and {500, 500, 500}, at 10m/s
"));
allCommands.Add(new Command(@"Weld, <name> : Weld blocks on a friendly ship
Example - [ Weld, Station ] - Weld blocks on ""Station""
"));
allCommands.Add(new Command(@"Weld, <name>, Fetch : ""Fetch"" tells the autopilot to keep track of components that need to be added to the welded ship and the next time the autopilot lands it will attempt to retrieve those components.
Example - [ Weld, Station, Fetch ; B Connector ; L Connector ; G Factory ] - Weld blocks on ""Station"", then land on ""Factory"" and retrieve needed components.
"));
allCommands.Add(new Command(@"[b]Directions[/b] : can be { Forward, Backward, Leftward, Rightward, Upward, Downward }. Autopilot only checks the first letter, so abbreviations will work. For example, ""Forward"" can be ""Fore"" or ""F""
"));
allCommands.Add(new Command(@"[b]Distances[/b] : for F, O, and P can be modified by km(kilometres) or Mm(megametres). For example, ""3.5M"" or ""3.5Mm"" is 3.5 megametres or 3 500 kilometres.
"));
			
			foreach (Command current in allCommands)
			{
				LinkedList<Command> bucket;
				if (help_messages.TryGetValue(current.command, out bucket))
				{
					myLogger.debugLog("adding to existing bucket: " + current.command, Logger.severity.TRACE);
					bucket.AddLast(current);
				}
				else
				{
					myLogger.debugLog("creating new bucket for: " + current.command, Logger.severity.TRACE);
					bucket = new LinkedList<Command>();
					bucket.AddLast(current);
					help_messages.Add(current.command, bucket);
				}
			}
			initialized = true;
		}

		public void printCommand(string message)
		{
			try
			{
				if (!initialized)
					initialize();

				if (string.IsNullOrWhiteSpace(message))
					printListCommands();
				else
					printSingleCommand(message);
			}
			catch (Exception e)
			{ myLogger.alwaysLog("Exception: " + e, Logger.severity.ERROR); }
		}

		private void printListCommands()
		{
			myLogger.debugLog("entered printListCommands()", Logger.severity.TRACE);
			StringBuilder print = new StringBuilder();
			foreach (LinkedList<Command> bucket in help_messages.Values)
			{
				myLogger.debugLog("printing a bucket.", Logger.severity.TRACE);
				foreach (Command current in bucket)
				{
					myLogger.debugLog("sending message for command: " + current.command, Logger.severity.TRACE);
					print.AppendLine(current.commandExt);
				}
			}
			myLogger.debugLog("showing mission screen for all", Logger.severity.TRACE);
			MyAPIGateway.Utilities.ShowMissionScreen("Autopilot Help", "Help Topics", string.Empty, print.ToString());
		}

		private bool printSingleCommand(string command)
		{
			myLogger.debugLog("entered printSingleCommand()", Logger.severity.TRACE);
			StringBuilder print = new StringBuilder();
			if (command.Equals("direction", StringComparison.OrdinalIgnoreCase) || command.Equals("distance", StringComparison.OrdinalIgnoreCase))
				command += 's';
			command = command.ToUpper();

			LinkedList<Command> bucket;
			if (!help_messages.TryGetValue(command, out bucket))
			{
				myLogger.alwaysLog("failed to get a bucket for: " + command, Logger.severity.WARNING);
				return false;
			}
			foreach (Command current in bucket)
			{
				myLogger.debugLog("sending message for command: " + current.command, Logger.severity.TRACE);
				print.AppendLine(current.description);
				print.AppendLine();
			}
			myLogger.debugLog("showing mission screen for command: " + command, Logger.severity.TRACE);
			MyAPIGateway.Utilities.ShowMissionScreen("Autopilot Help", command, string.Empty, print.ToString());
			return true;
		}
	}
}
